#!/usr/bin/env node
'use strict'

const pump = require("pump");
const split2 = require("split2");
const createPool = require("mysql2").createPool;
const db = require("./db.js").db;
const { Writable } = require('stream')

let connectionPool;

const dbLogStream = new Writable({
    write: function (chunk, encoding, next) {
        log(chunk.toString())
        next();
    }
});

// Startup process
const init = () => {
    const argsAreOk = checkArgs(process.argv);

    if (!argsAreOk) {
        return false;
    }

    const connectionDetails = extractConnectionDetailsFromString(process.argv[3])
    connectionPool = createPool({
        connectionLimit: 10,
        ...connectionDetails,
    });

    try {
        pump(process.stdin, split2(), dbLogStream)
    } catch (error) {
        log("{message: 'Something went wrong with the logger', error: " + error + "}")
    }
}

const log = (data) => {
    // Make a mysql compatibale iso8601 datetime string
    const datetime = (new Date).toISOString().substring(0, 19).replace('T', ' ');

    const insertQuery = "INSERT INTO system_logs (content, timestamp) VALUES (?, ?)";
    const insertValues = [data, datetime];

    db(connectionPool).query(insertQuery, insertValues).catch((error) => {
        error(error)
    });
}

// Convert a mysql connection url to an object
const extractConnectionDetailsFromString = (connectionString) => {
    try {
        const connection = {
            user: connectionString.split("mysql://")[1].split(":")[0],
            password: connectionString.split("mysql://")[1].split(":")[1].split("@")[0],
            host: connectionString.split("mysql://")[1].split(":")[1].split("@")[1],
            port: connectionString.split("mysql://")[1].split(":")[2].split("/")[0],
            database: connectionString.split("mysql://")[1].split(":")[2].split("/")[1],
        }

        return connection;
    } catch (err) {
        error(`Something went wrong, could not pase the connection string. Check your --url argument.

This error can happen if your username/password/host/database contains any of the following characters:
  / : @
Please update your details to accommodate for this.
`)
    }

    return {
        username: "",
        password: "",
        host: "",
        port: "",
        database: "",
    };
}

// Control that args are valid
const checkArgs = (args) => {
    let gotValidArgs = false;
    let errorDisplayed = false;

    if (args.length === 2) {
        error("No arguments given, expected 1");
        errorDisplayed = true;
    }

    if (args[2] === "--help") {
        help()
        errorDisplayed = true;
    }

    if (args[2] === "--url") {
        gotValidArgs = true;

        if (args.length !== 4 || args[3] === "") {
            gotValidArgs = false;
            error("Something went wrong, please check your --url argument.");
            errorDisplayed = true;
        }
    }

    if (!gotValidArgs && !errorDisplayed) {
        error("Something went wrong, please check your arguments.");
    }

    return gotValidArgs;
}

// Display error text
const error = (errorText) => {
    console.log("")
    console.log("🛑 Error:")
    console.error("🛑 " + errorText);
    console.log("")
    help();
}

// Display help text
const help = () => {
    console.log(`
---------------------
|                   |
|  > PINO MYSQL2 <  |
|                   |
---------------------

Usage:

--url {{URL}}
    Description: MySQL connection URL string to use
    Example: pino-mysql2 --url "mysql://username:password@host:port/database"

--help
    Description: Show this help text
`)
}

// Start the program
init();