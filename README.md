# pino-mysql2

A [Pino Transport](https://getpino.io/#/docs/transports) for MySQL and MariaDB Databases

![pino-mysql2](pino-mysql2.png)

## Differences between [pino-mysql2](https://gitlab.com/datahjelpen/public/extensions/pino-mysql2) and [pino-mysql](https://github.com/theproductiveprogrammer/pino-mysql)

- pino-mysql2 uses the [mysql2](https://www.npmjs.com/package/mysql2) package instead of the [mysql](https://www.npmjs.com/package/mysql) package.
- only using parameters, no configuration file
- table structure is not configurable
